"""Build vocabularies of words and tags from datasets"""
from collections import Counter
import json


# Hyper parameters for the vocab
PAD_WORD = '<pad>'
PAD_TAG = 'O'
UNK_WORD = 'UNK'

min_count_word = 2

def save_vocab_to_txt_file(vocab, txt_path):
    #Writes one token per line, 0-based line id corresponds to the id of the token.

    with open(txt_path, "w") as f:
        for token in vocab:
            f.write(token + '\n')


def save_dict_to_json(d, json_path):
    with open(json_path, 'w') as f:
        d = {k: v for k, v in d.items()}
        json.dump(d, f, indent=4)


def update_vocab(txt_path, vocab):
    #Update word and tag vocabulary from dataset; returns the number of elements in the dataset

    with open(txt_path) as f:
        for i, line in enumerate(f):
            vocab.update(line.strip().split(' '))

    return i + 1


if __name__ == '__main__':
    print("Building word vocabulary...")
    words = Counter()
    size_train_sentences = update_vocab('./data/train_sentences.txt', words)
    size_dev_sentences = update_vocab('./data/val_sentences.txt', words)
    size_test_sentences = update_vocab('./data/test_sentences.txt', words)


    print("Building tag vocabulary...")
    tags = Counter()
    size_train_tags = update_vocab('./data/train_labels.txt', tags)
    size_dev_tags = update_vocab('./data/val_labels.txt', tags)
    size_test_tags = update_vocab('./data/test_labels.txt', tags)

    # Assert same number of examples in datasets
    assert size_train_sentences == size_train_tags
    assert size_dev_sentences == size_dev_tags
    assert size_test_sentences == size_test_tags

    # Only keep most frequent tokens
    words = [tok for tok, count in words.items() if count >= min_count_word]
    tags = [tok for tok, count in tags.items()]

    # Add pad tokens
    if PAD_WORD not in words: words.append(PAD_WORD)
    if PAD_TAG not in tags: tags.append(PAD_TAG)

    # add word for unknown words
    words.append(UNK_WORD)

    # Save vocabularies to file
    print("Saving vocabularies to file...")
    save_vocab_to_txt_file(words, './data/words.txt')
    save_vocab_to_txt_file(tags, './data/tags.txt')
    print("- done.")

    # Save datasets properties in json file
    sizes = {'train_size': size_train_sentences, 'dev_size': size_dev_sentences, 'test_size': size_test_sentences, 'vocab_size': len(words), 'number_of_tags': len(tags), 'pad_word': PAD_WORD,
        'pad_tag': PAD_TAG, 'unk_word': UNK_WORD}
    save_dict_to_json(sizes, './data/dataset_params.json')

    # Logging sizes
    to_print = "\n".join("- {}: {}".format(k, v) for k, v in sizes.items())
print("Characteristics of the dataset:\n{}".format(to_print))
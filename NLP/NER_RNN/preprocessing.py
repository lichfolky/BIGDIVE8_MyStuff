import pandas as pd
import string as string

def createSentencesAndLabels(file, prefix):
    csv_file = pd.read_csv(file, sep=',')

    sentences=[]
    labels=[]

    #labels = csv_file['text']
    previd, label, txt = -1, [], ''
    for index, row in csv_file.iterrows():
        id=row['id']

        if(id != previd):
            if(len(txt) > 0):
                sentences.append(txt)
                labels.append(' '.join(label))

            txt = row['text']
            sz = len(txt.split(' '))
            label = ['O' for i in range(sz)]

        type=row['type']
        start=row['start']
        end=row['end']

        for i in range(start,end+1):
            label[i] = 'B-'+type if i == start else 'I-'+type

        previd=id

    with open("./data/"+prefix+"_sentences.txt", "w") as f:
        f.writelines(s + "\n" for s in sentences)
        f.close()

    with open("./data/"+prefix+"_labels.txt", "w") as f:
        f.writelines(str(l) + "\n" for l in labels)
        f.close()



def normalizeText(txt):
     return txt.translate(None, string.punctuation).lower()



if __name__ == '__main__':
    createSentencesAndLabels("./data/train.csv","train")
    createSentencesAndLabels("./data/dev.csv","val")
    createSentencesAndLabels("./data/test.csv","test")
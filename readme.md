## BigDive_MyStuff
Those are my exercises and notes of the BigDive, a four-week intensive course about Scientific Python, Data Science and DataViz.

### Table of contents
---

+ Scientific Python
	+ Numpy
	+ Pandas
+ Hadoop and Spark
 + PySpark
+ Mongodb
+ NLP
+ DataViz
	+ Matplotlib
	+ Seaborn
	+ D3.js
+ Geospatial Data
  + geopandas
  + Wasdi
+ Tools
	+ Git
	+ Docker
	+ Jupyter Notebooks
  + AWS for Data
